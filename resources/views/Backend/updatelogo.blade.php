@extends('backend.master')
@section('content')

    @section('site-title')
        Admin | Update Logo
    @endsection
    @section('page-main-title')
        Update Logo
    @endsection

    <!-- Content wrapper -->
    <div class="content-wrapper">
        <div class="container-xxl flex-grow-1 container-p-y">
            <div class="col-xl-12">
                <!-- File input -->
                <form action="/dashboard/updatelogo" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="card">
                        <div class="card-body">
                                <div class="mb-3 col-6">
                                    
                                    <label for="formFile" class="form-label ">Update <span class="text-danger">Recommend image size 190 x 40 pixels.</span></label>
                                    <input class="form-control" type="file" name="thumbnail" />
                                    <input type="hidden" name="id" value="{{$logo->id}}">
                                </div>
                            </div>
                            <div class="mb-3 mx-4">
                                <input type="submit" class="btn btn-primary" value="Update Post">
                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>

@endsection
