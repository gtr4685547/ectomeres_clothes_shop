@extends('backend.master')
@section('content')
<div class="content-wrapper">
    @section('site-title')
      Admin | View News
    @endsection
    @section('page-main-title')
      View News
    @endsection

    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="card">
          <div class="table-responsive text-nowrap">
            <table class="table">
              <thead>
                <tr>
                  <th>id</th>
                  <th>Title</th>
                  <th>Thumbnail</th>
                  <th>Decription</th>
                  <th>Date</th>
                  <th>Viewer</th>
                  <th>Created_at</th>
                  <th>Update_at</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody class="table-border-bottom-0">
                @foreach ($view_news as $news)
                    
                <tr>
                  <td>{{$news->id}}</td>
                  <td>{{$news->title}}</td>
                  <td><img src="../assets/image-news/{{$news->thumnail}}" width="100px" height="100px" alt=""></td>
                  <td><p>{{$news->description}}</p></td>
                  <td>{{$news->created_at}}/td>
                  <td>{{$news->viewer}}</td>
                  <td>{{$news->created_at}}</td>
                  <td>{{$news->updated_at}}</td>
                  <td>
                    <div class="dropdown">
                      <button type="button" class="btn p-0 dropdown-toggle hide-arrow" data-bs-toggle="dropdown">
                        <i class="bx bx-dots-vertical-rounded"></i>
                      </button>
                      <div class="dropdown-menu">
                        <a class="dropdown-item" href="/dashboard/updatenews{{$news->id}}"><i class="bx bx-edit-alt me-1"></i> Edit</a>
                        <a class="dropdown-item" id="remove-post-key" data-value="{{$news->id}}" data-bs-toggle="modal" data-bs-target="#basicModal" href=""><i class="bx bx-trash me-1"></i> Delete</a>
                      </div>
                    </div>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
        <div class="mt-3">
          <form action="/dashboard/deletenews" method="post">
            @csrf
            @method('DELETE')
          <div class="modal fade" id="basicModal" tabindex="-1" style="display: none;" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel1">Are you sure to remove this post?</h5>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-footer">
                  <input type="hidden" id="remove-val" name="remove_news">
                  <button type="submit" class="btn btn-danger">Confirm</button>
                  <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Cancel</button>
                </div>
              </div>
            </div>
          </form>
        </div>
        <div class="col-12">
          <ul class="pagination">
            {{-- @for ($i = 1; $i <=$total_page; $i++) --}}
            <li>
              {{-- <a href="/dashboard/viewproduct?page={{$i}}">{{$i}}</a> --}}
          </li>
            {{-- @endfor --}}
          </ul>
        </div>
      <hr class="my-5" />
    </div>
    <!-- / Content -->
  </div>
</div>
@endsection