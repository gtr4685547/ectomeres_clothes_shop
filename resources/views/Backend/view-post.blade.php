@extends('backend.master')
@section('content')
<div class="content-wrapper">
    @section('site-title')
      Admin | View Product
    @endsection
    @section('page-main-title')
      View Product
    @endsection

    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="card">
          <div class="table-responsive text-nowrap">
            <table class="table">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Requarl_Price</th>
                  <th>Sell_Price</th>
                  <th>QTY</th>
                  <th>Thubmnail</th>
                  <th>Category</th>
                  <th>Color</th>
                  <th>Size</th>
                  <th>Viewer</th>
                  <th>Cedit</th>
                  <th>Description</th>
                  <th>Created_at</th>
                  <th>Update_at</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody class="table-border-bottom-0">
                @foreach ($product as $pro)
                    
                <tr>
                    <td>{{$pro->name}}</td>
                    <td>{{$pro->reqular_price}}</td>
                    <td>{{$pro->sell_price}}</td>
                    <td>{{$pro->qty}}</td>
                    <td><img src="../assets/image-product/{{$pro->thumnail}}" width="100px" height="100px" alt=""></td>
                    <td>{{$pro->categ_name}}</td>
                    <td>{{$pro->color}}</td>
                    <td>{{$pro->size}}</td>
                    <td>{{$pro->viewer}}</td>
                    <td>{{$pro->users_name}}</td>
                    <td>{{$pro->description}}</td>
                    <td>{{$pro->created_at}}</td>
                    <td>{{$pro->updated_at}}</td>
                  <td>
                    <div class="dropdown">
                      <button type="button" class="btn p-0 dropdown-toggle hide-arrow" data-bs-toggle="dropdown">
                        <i class="bx bx-dots-vertical-rounded"></i>
                      </button>
                      <div class="dropdown-menu">
                        <a class="dropdown-item" href="{{route('goupdate_product', $pro->id)}}"><i class="bx bx-edit-alt me-1"></i> Edit</a>
                        <a class="dropdown-item" id="remove-post-key" data-value="{{$pro->id}}" data-bs-toggle="modal" data-bs-target="#basicModal" href=""><i class="bx bx-trash me-1"></i> Delete</a>
                      </div>
                    </div>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
        <div class="mt-3">
          <form action="/dashboard/deleteproduct" method="post">
            @csrf
            @method('DELETE')
          <div class="modal fade" id="basicModal" tabindex="-1" style="display: none;" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel1">Are you sure to remove this post?</h5>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-footer">
                  <input type="hidden" id="remove-val" name="remove_pro">
                  <button type="submit" class="btn btn-danger">Confirm</button>
                  <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Cancel</button>
                </div>
              </div>
            </div>
          </form>
        </div>
        <div class="col-12">
          <ul class="pagination">
            @for ($i = 1; $i <=$total_page; $i++)
            <li>
              <a href="/dashboard/viewproduct?page={{$i}}">{{$i}}</a>
          </li>
            @endfor
          </ul>
        </div>
      <hr class="my-5" />
    </div>
    <!-- / Content -->
  </div>
</div>

@endsection