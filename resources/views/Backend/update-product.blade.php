@extends('backend.master')
@section('content')

    @section('site-title')
        Admin | Update Product
    @endsection
    @section('page-main-title')
        Update Product
    @endsection

    <!-- Content wrapper -->
    <div class="content-wrapper">
        <div class="container-xxl flex-grow-1 container-p-y">
            <div class="col-xl-12">
                <!-- File input -->
                
                <form action="/dashboard/updateproduct" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="card">
                        
                        <div class="card-body">
                            
                            <div class="row">
                                <div class="mb-3 col-6">
                                    <label for="formFile" class="form-label">Name</label>
                                    <input class="form-control" type="text" name="name" value="{{$update_pro->name}}"  />
                                </div>
                                <div class="mb-3 col-6">
                                    <label for="formFile" class="form-label">Quantity</label>
                                    <input class="form-control" type="text" name="qty" value="{{$update_pro->qty}}" />
                                </div>
                                <div class="mb-3 col-6">
                                    <label for="formFile" class="form-label">Regular Price</label>
                                    <input class="form-control" type="number" name="regular_price" value="{{$update_pro->reqular_price}}" />
                                </div>
                                <div class="mb-3 col-6">
                                    <label for="formFile" class="form-label">Sale Price</label>
                                    <input class="form-control" type="number" name="sale_price" value="{{$update_pro->sell_price}}" />
                                </div>
                                <div class="mb-3 col-6">
                                    <label for="formFile" class="form-label">Available Size</label>
                                    <select name="size[]" class="form-control size-color" multiple="multiple">
                                        <option value="S" @foreach ($arrsize as $size)
                                            @if ($size== 'S') selected @endif
                                        @endforeach 
                                        >S</option>
                                        <option value="M" @foreach ($arrsize as $size)
                                            @if ($size== 'M') selected @endif
                                        @endforeach 
                                        >M</option>
                                        <option value="L" @foreach ($arrsize as $size)
                                            @if ($size== 'L') selected @endif
                                        @endforeach 
                                        >L</option>
                                        <option value="XL" @foreach ($arrsize as $size)
                                            @if ($size== 'XL') selected @endif
                                        @endforeach 
                                        >XL</option>
                                        <option value="XXL" @foreach ($arrsize as $size)
                                            @if ($size== 'XXL') selected @endif
                                        @endforeach 
                                        >XXL</option>
                                    </select>
                                </div>
                                <div class="mb-3 col-6">
                                    <label for="formFile" class="form-label">Available Color</label>
                                    <select name="color[]" class="form-control size-color" multiple="multiple">
                                        <option value="Red" @foreach ($arrcolor as $color)
                                            @if ($color== 'Red') selected @endif
                                        @endforeach 
                                        >Red</option>
                                        <option value="Blue" @foreach ($arrcolor as $color)
                                            @if ($color== 'Blue') selected @endif
                                        @endforeach 
                                        >Blue</option>
                                        <option value="Grey" @foreach ($arrcolor as $color)
                                            @if ($color== 'Grey') selected @endif
                                        @endforeach 
                                        >Grey</option>
                                        <option value="Black" @foreach ($arrcolor as $color)
                                            @if ($color== 'Black') selected @endif
                                        @endforeach 
                                        >Black</option>
                                    </select>
                                </div>
                                <div class="mb-3 col-6">
                                    <label for="formFile" class="form-label">Category</label>
                                    <select name="category" class="form-control">
                                            <option value="{{$update_pro->catigory_id}}">{{$update_pro->catigory_id}}</option>
                                    </select>
                                </div>
                                <div class="mb-3 col-6">
                                    <label for="formFile" class="form-label text-danger">Recommend image size ..x.. pixels.</label>
                                    <input class="form-control" value="{{$update_pro->thumnail}}" type="file" name="thumbnail" />
                                    <input type="text" name="old_pic" value="{{$update_pro->thumnail}}">
                                </div>
                                <div class="mb-3 col-12">
                                    <label for="formFile" class="form-label text-danger">Description</label>
                                    <textarea name="description" class="form-control" cols="30" rows="10">
                                        {{$update_pro->description}}
                                    </textarea>
                                </div>
                                
                            </div>
                            <div class="mb-3">
                                <input type="submit" class="btn btn-primary" value="Update">
                                <input type="hidden" name="post" value="{{$update_pro->id}}">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
