@extends('backend.master')
@section('content')

    @section('site-title')
        Admin | Update Category
    @endsection
    @section('page-main-title')
        Update Category
    @endsection

    <!-- Content wrapper -->
    <div class="content-wrapper">
        <div class="container-xxl flex-grow-1 container-p-y">
            <div class="col-xl-12">    
                
                <!-- File input -->
                <form action="/dashboard/updatecategory" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="card">
                        <div class="card-body">
                                <div class="mb-3 col-6">
                                    <label for="formFile" class="form-label ">Update <span class="text-danger"></span></label>
                                    <input class="form-control" type="text" name="categ_up" value="{{$category->name}}" />
                                    <input type="hidden" name="categ" value="{{$category->id}}">
                                </div>
                            </div>
                            <div class="mb-3 mx-4">
                                <input type="submit" class="btn btn-primary" value="Update Post">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
