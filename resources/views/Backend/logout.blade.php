@extends('backend.master')
@section('content')
<div class="content-wrapper">
    @section('site-title')
      Admin | Logout
    @endsection
    @section('page-main-title')
      Logout
    @endsection

    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="card p-5">
            <h1>Are u sure want to logout?</h1>
            <form  action="/logout" method="post">
                @csrf   
                <a href="/dashboard" class="btn btn-success">No</a>
                <button class="btn btn-danger">Yes</button>
            </form>
        </div>
    </div>
</div>
@endsection
