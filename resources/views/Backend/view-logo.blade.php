@extends('backend.master')
@section('content')
<div class="content-wrapper">
    @section('site-title')
      Admin | View Logo
    @endsection
    @section('page-main-title')
      View Logo
    @endsection

    <div class="container-xxl flex-grow-1 container-p-y">
      @if (Session::has('success'))
      <p class="text-success text-center">{{ Session::get('success') }}</p>
      @endif
      @if (Session::has('up-success'))
      <p class="text-success text-center">{{ Session::get('up-success') }}</p>
      @endif
      @if (Session::has('de-success'))
      <p class="text-success text-center">{{ Session::get('de-success') }}</p>
      @endif
        <div class="card">
          <div class="table-responsive text-nowrap">
            <table class="table">
              <thead>
                <tr>
                  <th>LOGO</th>
                  <th>Created_at</th>
                  <th>Updated_at</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody class="table-border-bottom-0">
                @foreach ($logo as $item)
                    
                
                <tr>
                  <td><img src="../assets/logo/{{$item->thumnail}}" width="100px" height="100px" alt=""></td>
                  <td>{{$item->created_at}}</td>
                  <td>{{$item->updated_at}}</td>
                  <td>
                    <div class="dropdown">
                      <button type="button" class="btn p-0 dropdown-toggle hide-arrow" data-bs-toggle="dropdown">
                        <i class="bx bx-dots-vertical-rounded"></i>
                      </button>
                      <div class="dropdown-menu">
                        <a class="dropdown-item" href="/dashboard/updatelogo/id={{$item->id}}"><i class="bx bx-edit-alt me-1"></i> Edit</a>
                        <a class="dropdown-item" id="remove-post-key" data-value="{{$item->id}}" data-bs-toggle="modal" data-bs-target="#basicModal" href="javascript:void(0);"><i class="bx bx-trash me-1"></i> Delete</a>
                      </div>
                    </div>
                  </td>
                </tr>
                @endforeach
              </tbody>

            </table>
          </div>
        </div>

        <div class="mt-3">
          <form action="/dashboard/Deletelogo" method="post">
            @csrf
          <div class="modal fade" id="basicModal" tabindex="-1" style="display: none;" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel1">Are you sure to remove this post?</h5>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-footer">
                  <input type="hidden" id="remove-val" name="remove_id">
                  <button type="submit" class="btn btn-danger">Confirm</button>
                  <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Cancel</button>
                </div>
              </div>
            </div>
          </form>
        </div>
        
      <hr class="my-5" />
    </div>
    <!-- / Content -->
  </div>
</div>

@endsection
