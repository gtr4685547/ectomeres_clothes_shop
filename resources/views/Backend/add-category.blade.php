@extends('backend.master')
@section('content')

    @section('site-title')
        Admin | Add Category
    @endsection
    @section('page-main-title')
        Add Category
    @endsection

    <!-- Content wrapper -->
    <div class="content-wrapper">
        <div class="container-xxl flex-grow-1 container-p-y">
            <div class="col-xl-12">
                <!-- File input -->
                <form action="/dashboard/addcategory" method="post" enctype="multipart/form-data">
                    @csrf
                    @if (Session::has('addsuccess')){
                        <script>
                            $(document).ready(function(){
                                swal({
                                title: "Good job!",
                                text: "You clicked the button!",
                                icon: "success",
                                button: "Aww yiss!",
                                });
                            })
                        </script>
                    }   
                    @endif
                    @if (Session::has('addfail')){
                        <script>
                            $(document).ready(function(){
                                swal({
                                title: "Error!",
                                text: "You clicked the button!",
                                icon: "error",
                                button: "ok",
                                });
                            })
                        </script>
                    }   
                    @endif
                    
                    <div class="card">
                        <div class="card-body">
                                <div class="mb-3 col-6">
                                    <label for="formFile" class="form-label ">Add Category <span class="text-danger"></span></label>
                                    <input class="form-control" type="text" name="category" placeholder="fill in the category"/>
                                </div>
                            </div>
                            <div class="mb-3 mx-4">
                                <input type="submit" class="btn btn-primary" value="Add Category">
                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>

@endsection
