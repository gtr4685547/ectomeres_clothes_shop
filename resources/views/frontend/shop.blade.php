        @extends('frontend.header')
        @section('title')
            Shop
        @endsection
        @section('content')

        
            <main class="shop">
                <section>
                    <div class="container">
                        <div class="row">
                            <div class="col-9">
                                <div class="row">
                                    @foreach ($product as $shop_pro)
                            
                        <div class="col-3">
                            <figure>
                                <div class="thumbnail">
                                    @if ($shop_pro->sell_price != 0)
                                    <div class="status">
                                        Promotion
                                    </div>
                                    @endif
                                    <a href="/prodocut-detail/id={{$shop_pro->id}}">
                                        <img src="../assets/image-product/{{$shop_pro->thumnail}}" alt="">
                                    </a>
                                </div>
                                <div class="detail">
                                    <div class="price-list">
                                        @if ($shop_pro->sell_price==0)
                                           <div class="price">US {{$shop_pro->reqular_price}}</div>
                                        @else
                                           <div class="regular-price "><strike>US {{$shop_pro->reqular_price}}</strike></div>
                                           <div class="sale-price ">US {{$shop_pro->sell_price}}</div>
                                        @endif
                                        
                                    </div>
                                    <h5 class="title">{{$shop_pro->name}}</h5>
                                </div>
                            </figure>
                        </div>
                        @endforeach
                                    <div class="col-12">
                                        <ul class="pagination">
                                            @if ($cateID)
                                            @for ($i = 1; $i <=$total_page; $i++)
                                            <li>
                                                <a href="/shop?page={{$i}}&cate={{$cateID}}">{{$i}}</a>
                                            </li>
                                            @endfor
                                            @elseif($price)
                                            @for ($i = 1; $i <=$total_page; $i++)
                                            <li>
                                                <a href="/shop?page={{$i}}&price={{$price}}">{{$i}}</a>
                                            </li>
                                            @endfor
                                            @elseif($promotion)
                                            @for ($i = 1; $i <=$total_page; $i++)
                                            <li>
                                                <a href="/shop?page={{$i}}&promotion=true">{{$i}}</a>
                                            </li>
                                            @endfor
                                            @else
                                                @for ($i = 1; $i <=$total_page; $i++)
                                                <li>
                                                    <a href="/shop?page={{$i}}&cate=">{{$i}}</a>
                                                </li>
                                                @endfor
                                            @endif
                                        </ul>
                                    </div>
                                
                                </div>
                            </div>
                            <div class="col-3 filter">
                                <h4 class="title">Category</h4>
                                <ul>
                                    <li>
                                        <a href="/shop">ALL</a>
                                    </li>
                                    @foreach ( $category as $item)
                                    <li>
                                        <a href="/shop?page=1&cate={{$item->id}}">{{$item->name}}</a>
                                    </li>
                                    @endforeach
                                </ul>
                                
                                <h4 class="title mt-4">Price</h4>
                                <div class="block-price mt-4">
                                    <a href="/shop?price=max">High</a>
                                    <a href="/shop?price=min">Low</a>
                                </div>
            
                                <h4 class="title mt-4">Promotion</h4>
                                <div class="block-price mt-4">
                                    <a href="/shop?promotion=true">Promotion Product</a>
                                </div>
            
                            </div>
                        </div>
                    </div>
                </section>
            
            </main>
        @endsection