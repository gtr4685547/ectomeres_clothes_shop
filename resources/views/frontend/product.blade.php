@extends('frontend.header')
@section('title')
    Prdouct Detail
@endsection
@section('content')

        <main class="product-detail">

            <section class="review">
                <div class="container">
                    <div class="row">
                        <div class="col-5">
                            <div class="thumbnail">
                                <img src="{{url('assets/image-product/'.$product->thumnail)}}" alt="" width="450px">
                            </div>
                        </div>
                        <div class="col-7">
                            <div class="detail">
                                <div class="price-list">
                                    @if ($product->sell_price == 0)
                                        <div class="price">US {{$product->reqular_price}}</div>
                                    @else
                                        <div class="regular-price"><strike> US {{$product->reqular_price}}</strike></div>
                                        <div class="sale-price">US {{$product->sell_price}}</div>
                                    @endif
                                </div>
                                <h5 class="title">Plain T-shirt</h5>
                                <div class="group-size">
                                    <span class="title">Color Available</span>
                                    <div class="group">
                                        {{$product->color}}
                                    </div>
                                </div>
                                <div class="group-size">
                                    <span class="title">Size Available</span>
                                    <div class="group">
                                        {{$product->size}}
                                    </div>
                                </div>
                                <div class="group-size">
                                    <span class="title">Description</span>
                                    <div class="description">
                                        {{$product->description}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        
            <section>
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <h3 class="main-title">
                                RELATED PRODUCTS
                            </h3>
                        </div>
                    </div>
                    <div class="row">
                        @foreach ($relate as $product)
                        <div class="col-3">
                        <figure>
                            <div class="thumbnail">
                                @if ($product->sell_price != 0)
                                <div class="status">
                                    Promotion
                                </div>
                                @endif
                                <a href="/prodocut-detail/id={{$product->id}}">
                                    <img src="../assets/image-product/{{$product->thumnail}}" alt="">
                                </a>
                            </div>
                            <div class="detail">
                                <div class="price-list">
                                    @if ($product->sell_price==0)
                                       <div class="price">US {{$product->reqular_price}}</div>
                                    @else
                                       <div class="regular-price "><strike>US {{$product->reqular_price}}</strike></div>
                                       <div class="sale-price ">US {{$product->sell_price}}</div>
                                    @endif
                                    
                                </div>
                                <h5 class="title">{{$product->name}}</h5>
                            </div>
                        </figure>
                    </div>
                    @endforeach
                    </div>
                </div>
            </section>
        </main>
        @endsection

   