        @extends('frontend.header')
        @section('title')
            Home
        @endsection
        @section('content')
            
        
        <main class="home">
            <section>
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <h3 class="main-title">
                                NEW PRODUCTS
                            </h3>
                        </div>
                    </div>
                    <div class="row">
                        @foreach ($newproduct as $product)
                            
                        <div class="col-3">
                            <figure>
                                <div class="thumbnail">
                                    @if ($product->sell_price != 0)
                                    <div class="status">
                                        Promotion
                                    </div>
                                    @endif
                                    <a href="/prodocut-detail/id={{$product->id}}">
                                        <img src="../assets/image-product/{{$product->thumnail}}" alt="">
                                    </a>
                                </div>
                                <div class="detail">
                                    <div class="price-list">
                                        @if ($product->sell_price==0)
                                           <div class="price">US {{$product->reqular_price}}</div>
                                        @else
                                           <div class="regular-price "><strike>US {{$product->reqular_price}}</strike></div>
                                           <div class="sale-price ">US {{$product->sell_price}}</div>
                                        @endif
                                        
                                    </div>
                                    <h5 class="title">{{$product->name}}</h5>
                                </div>
                            </figure>
                        </div>
                        @endforeach
                    </div>
                </div>
            </section>

            <section>
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <h3 class="main-title">
                                PROMOTION PRODUCTS
                            </h3>
                        </div>
                    </div>
                    <div class="row">
                        @foreach ($promotion as $dis)
                        <div class="col-3">
                            <figure>
                                <div class="thumbnail">
                                    <div class="status">
                                        Promotion
                                    </div> 
                                    <a href="/prodocut-detail/id={{$dis->id}}">
                                        <img src="../assets/image-product/{{$dis->thumnail}}" alt="">
                                    </a>
                                </div>
                                <div class="detail">
                                    <div class="price-list">
                                        <div class="regular-price "><strike> US {{$dis->reqular_price}}</strike></div>
                                        <div class="sale-price ">US {{$dis->sell_price}}</div>
                                    </div>
                                    <h5 class="title">{{$dis->name}}</h5>
                                </div>
                            </figure>
                        </div>
                        @endforeach
                    </div>
                </div>
            </section>  

            <section>
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <h3 class="main-title">
                                POPULAR PRODUCTS
                            </h3>
                        </div>
                    </div>
                    <div class="row">
                        @foreach ($popular as $product)
                            <div class="col-3">
                            <figure>
                                <div class="thumbnail">
                                    @if ($product->sell_price != 0)
                                    <div class="status">
                                        Promotion
                                    </div>
                                    @endif
                                    <a href="/prodocut-detail/id={{$product->id}}">
                                        <img src="../assets/image-product/{{$product->thumnail}}" alt="">
                                    </a>
                                </div>
                                <div class="detail">
                                    <div class="price-list">
                                        @if ($product->sell_price==0)
                                           <div class="price">US {{$product->reqular_price}}</div>
                                        @else
                                           <div class="regular-price "><strike>US {{$product->reqular_price}}</strike></div>
                                           <div class="sale-price ">US {{$product->sell_price}}</div>
                                        @endif
                                        
                                    </div>
                                    <h5 class="title">{{$product->name}}</h5>
                                </div>
                            </figure>
                        </div>
                        @endforeach
                    </div>
                </div>
            </section>
        </main> 
        @endsection
