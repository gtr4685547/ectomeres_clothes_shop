<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use SebastianBergmann\CodeCoverage\Report\Xml\Totals;

class productcontroller extends Controller
{
    public function Home(){
        $newproduct = DB::table('product')->orderBy('id','DESC')->limit(4)->get();
        $promotion = DB::table('product')->where('sell_price','!=','0')->orderBy('id','DESC')->limit(4)->get();
        $popular = DB::table('product')->orderBy('viewer','DESC')->limit(4)->get();
        return view('frontend.index',compact('newproduct','promotion','popular'));
    }
    public function News(){
        $news = DB::table('news')->orderBy('id','DESC')->limit(8)->get();
        return view('frontend.news',compact('news'));
    }
    public function newDetail($id){
        $news_detail = DB::table('news')->where('id',$id)->first();
        return view('frontend.news-detail',compact('news_detail'));
    }
    public function Shop(Request $request){
        $cateID = $request->cate;
        $price = $request->price;
        $promotion = $request->promotion;
        if($request->page){
            $page = $request->page;
        }
        else{
            $page  = 1;
        }
        if(!empty($cateID)){
        $total_product = DB::table('product')->where('catigory_id',$cateID)->count();
        $total_page = ceil($total_product/4);
        $pagine = ($page-1) * 4;
        $product = DB::table('product')->where('catigory_id',$cateID)->orderBy('id','DESC')->limit(4)->offset($pagine)->get();
        }
        else if($price == 'max'){
            $total_product = DB::table('product')->count();
            $total_page = ceil($total_product/4);
            $pagine = ($page-1) * 4;
            $product = DB::table('product')->orderBy('reqular_price','ASC')->limit(4)->offset($pagine)->get();
        }
        else if($price == 'min'){
            $total_product = DB::table('product')->count();
            $total_page = ceil($total_product/4);
            $pagine = ($page-1) * 4;
            $product = DB::table('product')->orderBy('reqular_price','DESC')->limit(4)->offset($pagine)->get();
        }
        else if($promotion){
            $total_product = DB::table('product')->where('sell_price','!=','0')->count();
            $total_page = ceil($total_product/4);
            $pagine = ($page-1) * 4;
            $product = DB::table('product')->where('sell_price','!=','0')->orderBy('reqular_price','DESC')->limit(4)->offset($pagine)->get();
        }
        else{
            $total_product = DB::table('product')->count();
            $total_page = ceil($total_product/4);
            $pagine = ($page-1) * 4;
            $product = DB::table('product')->orderBy('id','DESC')->limit(4)->offset($pagine)->get();
      
        }
        $category = DB::table('catigory')->orderBy('id','DESC')->get();
        return view('frontend.shop',compact('product','total_page','cateID','category','price','promotion'));
    }

    public function productDetail($id){
        $product = DB::table("product")->where('id',$id)->first();
        DB::table("product")->where('id',$id)->update([
            'viewer'=>$product->viewer+1,
        ]);
        $relate = DB::table('product')->where('catigory_id',$product->catigory_id)->whereNot('id',$id)->limit(4)->get();
        return view('frontend.product',compact('product','relate'));
    }

}
