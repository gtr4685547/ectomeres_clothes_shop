<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use function Laravel\Prompts\table;

class productbackController extends Controller
{
    public function viewproduct(Request $request){
       
        if($request->page)
        {
            $page = $request->page;
        }
        else{
            $page = 1;
        }
        $total_pro = DB::table('product')->count();
        $total_page = ceil( $total_pro/3);
        $pro = ($page-1)*3;
        $product = DB::table('product')->join('catigory','product.catigory_id', '=' ,'catigory.id')->select('product.*','catigory.name AS categ_name','users.name AS users_name')
                                      ->join('users','product.post_by' , '=' , 'users.id')
                                      ->orderBy('id','DESC')
                                      ->limit(3)->offset($pro)
                                      ->get();
       return view('Backend.view-post',compact('product','total_page'));
    }
    public function goadd_product(){
        $getcategory = DB::table('catigory')->get();
        return view('Backend.add-post',compact('getcategory'));
    }
    public function add_product(Request $request){
        $this->validate($request,[
            'name'=>'required',
            'qty'=>'required',
            'regular_price'=>'required',
            'sale_price'=>'required',
            'size'=>'required',
            'color'=>'required',
            'category'=>'required',
            'thumbnail'=>'required',
            'description'=>'required',
        ]);
        $name = $request->name;
        $qty = $request->qty;
        $regular_price = $request->regular_price;
        $sale_price = $request->sale_price;
        $size = $request->size;
        $color = $request->color;
        $category = $request->category;
        $description = $request->description;
        $strsize = implode(',',$size);
        $strcolor = implode(',',$color);
        if(empty($sale_price)){
            $sale_price = 0;
        }
        $author = Auth()->user()->id;

        $thumbnail = $request->thumbnail;
        if($thumbnail){
            $newthumnail = rand(1,9999).'-'.$thumbnail->getClientOriginalName();
            $path = 'assets/image-product';
            $thumbnail->move($path,$newthumnail);
        }

        DB::table('product')->insert([
            'name'=>$name,
            'reqular_price'=>$regular_price,
            'sell_price'=>$sale_price,
            'qty'=>$qty,
            'thumnail'=>$newthumnail,
            'catigory_id'=>$category,
            'color'=>$strcolor,
            'size'=>$strsize,
            'viewer'=>0,
            'post_by'=>$author,
            'description'=>$description,
            'created_at'=>date('y-m-d h:i:s'),
            'updated_at'=>date('y-m-d h:i:s'),
        ]);
        return redirect('/dashboard/viewproduct');
    }
    public function goupdate_product($up){    
        $update_pro = DB::table('product')->where('id',$up)->first();
        $arrsize = explode(',',$update_pro->size);
        $arrcolor = explode(',',$update_pro->color);
        // return $arrcolor;

        return view('Backend.update-product',compact('update_pro','arrsize','arrcolor'));
    }
    public function update_product(Request $request){
        $up_pro = $request->post;
        $name = $request->name;
        $qty = $request->qty;
        $regular_price = $request->regular_price;
        $sale_price = $request->sale_price;
        $size = $request->size;
        $color = $request->color;
        $category = $request->category;
        $description = $request->description;
        $strsize = implode(',',$size);
        $strcolor = implode(',',$color);
        if(empty($sale_price)){
            $sale_price = 0;
        }
        $thumbnail = $request->thumbnail;
        if($thumbnail){
            $newthumnail = rand(1,9999).'-'.$thumbnail->getClientOriginalName();
            $path = 'assets/image-product';
            $thumbnail->move($path,$newthumnail);
        }
        else{
            $newthumnail = $request->old_pic;
        }
        $update = DB::table('product')->where('id',$up_pro)->update([
            'name'=>$name,
            'reqular_price'=>$regular_price,
            'sell_price'=>$sale_price,
            'qty'=>$qty,
            'thumnail'=>$newthumnail,
            'catigory_id'=>$category,
            'color'=>$strcolor,
            'size'=>$strsize,
            'description'=>$description,
            'updated_at'=>date('y-m-d h:i:s'),
        ]);
        return redirect('/dashboard/viewproduct');
    }
    public function delete_product(Request $request){
        $del= $request->remove_pro;
        $delete = DB::table('product')->where('id',$del)->delete();
        return redirect('/dashboard/viewproduct');
    }
}
