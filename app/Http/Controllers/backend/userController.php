<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class userController extends Controller
{
    public function gologin(){
        return view('Backend.login');
    }
    public function login(Request $request){
        $email = $request->name_email;
        $password = $request->password ;

        if(Auth::attempt([
            'name' => $email,
            'password' => $password
        ])){
            return redirect('/dashboard');
        }
        else if(Auth::attempt([
            'email' => $email,
            'password'=>$password
        ]))
        {
            return redirect('/dashboard');
        }
        else{
            return redirect('/login')->with('error','invalid Something');
        }
    }
    public function goregister(){
        return view('Backend.register');
    }
    public function register(Request $request){
        $this->validate($request,[
            'name'=>'required',
            'email'=>'required',
            'password'=>'required',
            'profile'=>'required'
        ]);
        // return test;
        $name =$request->name;
        $email =$request->email;
        $passowrd =$request->password;
        $profile =$request->file('profile');

        // return $name.$email.

        if($profile){
            $newprofile = rand(1,9999).'-'.$profile->getClientOriginalName();
            $path = 'assets/adminprofile';
            $profile ->move($path,$newprofile);
        }
        DB::table('users')->insert([
            'name'=>$name,
            'email'=>$email,
            'password'=>Hash::make($passowrd),
            'profile'=>$newprofile,
            'created_at'=>date('y-m-d h:i:s'),
            'updated_at'=>date('y-m-d h:i:s')
        ]);
        return redirect('/dashboard');
    }
    public function logout(){
        Auth::logout();
        return redirect('/login');
    }
    public function gologout(){
        return view('backend.logout');
    }
}
