<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class newscontroller extends Controller
{
    public function viewnews(){
        $view_news = DB::table('news')->orderBy('id','ASC')->limit(5)->get();
        return view('Backend.view-news',compact('view_news'));
    }
    public function goadd_news(){
        return view('Backend.add-news');
    }
    public function add_news(Request $request){
        $this->validate($request,[
            'title'=>'required',
            'thumbnail'=>'required',
            'description'=>'required'
        ]);
        $title = $request->title;
        $description = $request->description;
        $author = Auth()->user()->id;

        $thumbnail = $request->thumbnail;
        if($thumbnail){
            $newthumnail = rand(1,9999).'-'.$thumbnail->getClientOriginalName();
            $path = 'assets/image-news';
            $thumbnail->move($path,$newthumnail);
        }
        $insert = DB::table('news')->insert([
            'title'=>$title,
            'thumnail'=>$newthumnail,
            'description'=>$description,
            'viewer'=>0,
            'post_by'=>$author,
            'created_at'=>date('y-m-d h:i:s'),
            'updated_at'=>date('y-m-d h:i:s'),
        ]);
        return redirect('/dashboard/viewnews');
    }
    public function goupdate_news($id){
        $update = DB::table('news')->where('id',$id)->first();
        return view('Backend.update-news',compact('update'));
    }
    public function update_news(Request $request){
        $update_news = $request->up_news;
        $thumbnail = $request->thumbnail;
        if($thumbnail){
            $newthumnail = rand(1,9999).'-'.$thumbnail->getClientOriginalName();
            $path = 'assets/image-news';
            $thumbnail->move($path,$newthumnail);
        }
        else{
            $newthumnail = $request->old_pic;
        }
        DB::table('news')->where('id',$update_news)->update([
            'title'=>$request->title,
            'description'=>$request->description,
            'thumnail'=>$thumbnail,
            'updated_at'=>date('y-m-d h:i:s'),
        ]);
        return redirect('/dashboard/viewnews');
    }
    public function delete(Request $request){
        $remove_news = $request->remove_news;
        $delete = DB::table('news')->where('id',$remove_news)->delete();
        return redirect('/dashboard/viewnews');
    
    }
}
