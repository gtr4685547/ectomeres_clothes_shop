<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class logoController extends Controller
{
    public function viewlogo(){
       $logo= DB::table('logo')->get();
        return view('Backend.view-logo',compact('logo'));
    }
    public function goadd(){
        return view('Backend.add-logo');
    }
    public function addlogo(Request $request){
        $this->validate($request,[
            'thumbnail'=>'required',
        ]);
        $thumnail = $request->thumbnail;
        if($thumnail){
            $newthumnail = rand(1,9999).'-'.$thumnail->getClientOriginalName();
            $path = 'assets/logo';
            $thumnail->move($path,$newthumnail);
        }
        DB::table('logo')->insert([
            'thumnail'=>$newthumnail,
            'created_at'=>date('y-m-d h:i:s'),
            'updated_at'=>date('y-m-d h:i:s'),
        ]);
        return redirect('/dashboard/viewlogo')->with('success','Logo Add success');
    }


    public function goupdate($id){
        $logo= DB::table('logo')->where('id',$id)->first();
         return view('Backend.updatelogo',compact('logo'));
    }
    public function updatelogo(Request $request){
        $up_id = $request->id;

        $this->validate($request,[
            'thumbnail'=>'required',
        ]);
        $thumnail = $request->thumbnail;
        if($thumnail){
            $newthumnail = rand(1,9999).'-'.$thumnail->getClientOriginalName();
            $path = 'assets/logo';
            $thumnail->move($path,$newthumnail);
        }

        $uplogo = DB::table('logo')->where('id',$up_id)->update([
            'thumnail' => $newthumnail,
            'updated_at'=>date('y-m-d h:i:s')
        ]);
        return redirect('/dashboard/viewlogo')->with('up-success','update logo success');
    }

    public function deletelogo(Request $request){
        $remove = $request->remove_id;
        DB::table('logo')->where('id',$remove)->delete();
        return redirect('/dashboard/viewlogo')->with('de-success','Delete logo success');
    }
}

