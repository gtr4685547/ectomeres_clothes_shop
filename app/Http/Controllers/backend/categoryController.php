<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class categoryController extends Controller
{
    public function viewcategory(){
        $getcategory = DB::table('catigory')->get();
        return view('Backend.view-category',compact('getcategory'));
    }
    public function goadd_category(){
        return view('Backend.add-category');
    }
    public function add_category(Request $request){
        $category = $request->category;
        if($category){
            DB::table('catigory')->insert([
                'name'=>$category,
                'created_at'=>date('y-m-d h:i:s'),
                'updated_at'=>date('y-m-d h:i:s')
            ]);
            return redirect('/dashboard/viewcategory')->with('addsuccess','');
        }
        else{
            return redirect('/dashboard/addcategory')->with('addfail','');
        }
        
    }
    public function goupdate_category($categ){
        
        $category= DB::table('catigory')->where('id',$categ)->first();
        return view('Backend.update-category',compact('category'));
    }
    public function update_category(Request $request){
        $id = $request->categ;
        $this->validate($request,[
            'categ_up'=>'required',
        ]);
        $up_category = $request->categ_up;
        $update = DB::table('catigory')->where('id',$id)->update([
            'name'=>$up_category,
            'updated_at'=>date('y-m-d h:i:s')
        ]);
        return redirect('/dashboard/viewcategory')->with('up_suc','Update catefgory success!');
    }

    public function delete_category(Request $request){
        $remove = $request->remove_categ;
        $delete_categ = DB::table('catigory')->where('id',$remove)->delete();
        return redirect('/dashboard/viewcategory')->with('delete_suc','');
    }
}