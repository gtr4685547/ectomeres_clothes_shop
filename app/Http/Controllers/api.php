<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class api extends Controller
{
    public function ConvertJosn($data){
        $respone = array([
            'code'=>200,
            'data'=>$data,
        ]);
        return $respone;
    }
    public function getlogo(){
       $logo = DB::table('logo')->get();
        return $this->ConvertJosn($logo);
    }
    public function addlogo(Request $request){
        $thumnail = $request->thumbnail;
        if($thumnail){
            $newthumnail = rand(1,9999).'-'.$thumnail->getClientOriginalName();
            $path = 'assets/logo';
            $thumnail->move($path,$newthumnail);
        }
        DB::table('logo')->insert([
            'thumnail'=>$thumnail,
            'created_at'=>date('y-m-d h:i:s'),
            'updated_at'=>date('y-m-d h:i:s'),
        ]);
        return $this->ConvertJson("add logo successfully");
    }

}
