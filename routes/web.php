<?php

use App\Http\Controllers\api;
use App\Http\Controllers\backend\AdminController;
use App\Http\Controllers\backend\categoryController;
use App\Http\Controllers\backend\logoController;
use App\Http\Controllers\Backend\newscontroller;
use App\Http\Controllers\backend\productbackController;
use App\Http\Controllers\backend\userController;
use App\Http\Controllers\frontend\productcontroller;
use Illuminate\Routing\RouteUri;
use Illuminate\Routing\RouteUrlGenerator;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('frontend.index');
// });


// Frontend route

Route::get('/',[productcontroller::class,'Home'])->name('Home');
Route::get('/shop',[productcontroller::class,'Shop'])->name('shop');
Route::get('/news',[productcontroller::class,'News'])->name('news');
Route::get('/prodocut-detail/id={id}',[productcontroller::class,'productDetail']);
Route::get('/new-detail/id={id2}',[productcontroller::class,'newDetail']);


// Backend
Route::get('/dashboard',[AdminController::class,'dashboard'])->name('dashboard');
Route::controller(userController::class)->group(function(){
    Route::get('/login' ,'gologin')->name('gologin');
    Route::post('/login' ,'login')->name('login');
    Route::get('/register' ,'goregister')->name('goregister');
    Route::post('/register' ,'register')->name('register');
    Route::get('/logout','gologout')->name('go-logout');
    Route::post('/logout','logout')->name('logout');
});
Route::controller(logoController::class)->group(function(){
    Route::get('/dashboard/viewlogo','viewlogo')->name('viewlogo');
    Route::get('/dashboard/goadd' , 'goadd' )->name('goadd');
    Route::post('/dashboard/addlogo' , 'addlogo' )->name('addlogo');
    Route::get('/dashboard/updatelogo/id={id}','goupdate')->name('goupdate');
    Route::post('/dashboard/updatelogo' ,'updatelogo' )->name('updatelogo');
    Route::post('/dashboard/Deletelogo','deletelogo')->name('deletelogo');

});


// category

Route::controller(categoryController::class)->group(function(){
    Route::get('/dashboard/viewcategory','viewcategory')->name('viewcategory');
    Route::get('/dashboard/addcategory','goadd_category')->name('goadd_category');
    Route::post('/dashboard/addcategory','add_category')->name('add_category');
    Route::get('/dashboard/updatecategory/categ={categ}','goupdate_category')->name('goupdate_category');
    Route::post('/dashboard/updatecategory','update_category')->name('update_category');
    Route::delete('/dashboard/deletecategory','delete_category')->name('delete_category');
});
Route::controller(productbackController::class)->group(function(){
    Route::get('/dashboard/viewproduct','viewproduct')->name('viewproduct');
    Route::get('/dashboard/addproduct','goadd_product')->name('goadd_product');
    Route::post('/dashboard/addproduct','add_product')->name('add_product');
    Route::get('/dashboard/updateproduct/up={up}','goupdate_product')->name('goupdate_product');
    Route::post('/dashboard/updateproduct','update_product')->name('update_product');
    Route::delete('/dashboard/deleteproduct','delete_product')->name('delete_product');
});

Route::controller(newscontroller::class)->group(function(){
    Route::get('/dashboard/viewnews','viewnews')->name('viewnews');
    Route::get('/dashboard/addnews','goadd_news')->name('goadd_news');
    Route::post('/dashboard/addnews','add_news')->name('add_news');
    Route::get('/dashboard/updatenews{id}','goupdate_news')->name('goupdate_news');
    Route::post('/dashboard/updatenews','update_news')->name('update_news');
    Route::delete('/dashboard/deletenews','delete')->name('delete_news');
});

